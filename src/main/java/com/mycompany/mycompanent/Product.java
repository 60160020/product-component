/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mycompanent;

import java.util.ArrayList;

/**
 *
 * @author Lenovo
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }

    public static ArrayList<Product> genProductList() {
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, "เอสเพรสโซ่", 40, "1.jpg"));
        list.add(new Product(2, "ลาเต้", 45, "2.jpg"));
        list.add(new Product(3, "มัทชะ กรีน ทรี", 50, "3.jpg"));
        list.add(new Product(4, "ชาไทย", 30, "4.jpg"));
        list.add(new Product(5, "อเมริกาโน่", 55, "5.jpg"));
        list.add(new Product(6, "คาปูชิโน", 40, "6.jpg"));
        list.add(new Product(7, "นมสดคาราเมล", 30, "7.jpg"));
        return list;
    }
}
